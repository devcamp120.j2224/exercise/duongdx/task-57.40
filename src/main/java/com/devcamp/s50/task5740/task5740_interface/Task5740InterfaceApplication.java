package com.devcamp.s50.task5740.task5740_interface;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Task5740InterfaceApplication {

	public static void main(String[] args) {
		SpringApplication.run(Task5740InterfaceApplication.class, args);
	}

}
