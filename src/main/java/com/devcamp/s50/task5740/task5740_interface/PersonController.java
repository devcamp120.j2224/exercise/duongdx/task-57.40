package com.devcamp.s50.task5740.task5740_interface;

import java.util.ArrayList;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PersonController {
    // Danh sach list student
    @CrossOrigin
    @GetMapping("/listStudent")
    public ArrayList<Person> getListPerson() {
        ArrayList<Person> persons = new ArrayList<Person>();
        ArrayList<pet> listPet = new ArrayList<>();
        ArrayList<Subject> listSubject = new ArrayList<>();
        pet dog = new pet("dog", "male");
        pet cat = new pet("cat", "female");
        listPet.add(dog);
        listPet.add(cat);

        Subject HoaDaiCuong = new Subject("Hoa Hoc ", 32, new Professor("Anh", 40, "male", new Address(), listPet));
        listSubject.add(HoaDaiCuong);
        
        Student student1 = new Student("Tuan", 26, "male", new Address(), listPet, 2, listSubject);
        Student student2 = new Student("Linh", 23, "female", new Address(), listPet);
        Worker worker1 = new Worker("Duong", 28, "male", new Address("HaiDuong", "ChiLinh", "saoDo", 34), listPet, 20000);
        Professor professor1 = new Professor("Thuan", 32, "male",new Address("VanCao", "HaiPhong", "VietNam", 16), listPet,20000);
        Professor professor2 = new Professor("Ten", 41, "famale", new Address(), listPet, 1000);
        professor2.gotoSchool();
        professor2.gotoShop();
        professor2.play();

        persons.add(student1);
        persons.add(student2);
        persons.add(worker1);
        persons.add(professor1);
        persons.add(professor2);

        return persons;
    }
}
