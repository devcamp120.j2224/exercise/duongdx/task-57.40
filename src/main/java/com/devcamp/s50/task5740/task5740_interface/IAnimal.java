package com.devcamp.s50.task5740.task5740_interface;

public interface IAnimal {
    public void annimalSound();
    public void sleep();

}
