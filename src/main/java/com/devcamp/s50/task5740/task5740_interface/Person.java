package com.devcamp.s50.task5740.task5740_interface;

import java.util.ArrayList;

public abstract class Person implements IAnimal,ILive{
    private String name;
    private int age ;
    private String gender;
    private Address address ;
    private ArrayList<pet> listPet ;

    public abstract void eat();

    public Person(String name, int age, String gender, Address address, ArrayList<pet> listPet) {
        this.name = name;
        this.age = age;
        this.gender = gender;
        this.address = address;
        this.listPet = listPet;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public ArrayList<pet> getListPet() {
        return listPet;
    }

    public void setListPet(ArrayList<pet> listPet) {
        this.listPet = listPet;
    }

    
}
