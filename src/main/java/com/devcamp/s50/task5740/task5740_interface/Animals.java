package com.devcamp.s50.task5740.task5740_interface;

public abstract class Animals implements IAnimal{
    private int age ;
    private String gender ;
    
    public Animals(int age, String gender) {
        this.age = age;
        this.gender = gender;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public abstract void isMammals();

    public void mate() {
        System.out.println("Animals mating");
    }
}
